// ChildCrashDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CrashApplication.h"
#include "ChildCrashDlg.h"
#include "afxdialogex.h"
#include "ErrorRaiser.h"
#include "ErrorHandler.h"

// ChildCrashDlg dialog

IMPLEMENT_DYNAMIC(ChildCrashDlg, CDialog)

ChildCrashDlg::ChildCrashDlg(CWnd* pParent /*=NULL*/)
	: CDialog(ChildCrashDlg::IDD, pParent)
{

}

ChildCrashDlg::~ChildCrashDlg()
{
}

void ChildCrashDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO1, m_exceptionsCombo);
	DDX_Control(pDX, IDC_CHECK1, m_inBgrThread);
}


BEGIN_MESSAGE_MAP(ChildCrashDlg, CDialog)
	ON_BN_CLICKED(IDC_BUTTON1, &ChildCrashDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &ChildCrashDlg::OnBnClickedButton2)
	ON_MESSAGE(WM_USER + 123, OnThreadSentData)
END_MESSAGE_MAP()


// ChildCrashDlg message handlers
LRESULT ChildCrashDlg::OnThreadSentData(WPARAM wParam, LPARAM lParam)
{
	return CThreadContainer::OnThreadSentData(wParam, lParam);
}

void ChildCrashDlg::CrashThreadFunction(IThread::TData dataPtr)
{
	global_error_handling::activateForThread();
	Sleep(4000);
	error_raiser::raise((error_raiser::ExceptionCodeEnum) m_exceptionsCombo.GetItemData(m_exceptionsCombo.GetCurSel()));
}

BOOL ChildCrashDlg::OnInitDialog()
{
	CDialog::OnInitDialog();
	error_raiser::fillCombo(&m_exceptionsCombo);
	m_exceptionsCombo.SetCurSel(0);


	AddThread(new CWinApiThread(GetSafeHwnd(), WM_USER + 123, delegates::make(this, &ChildCrashDlg::CrashThreadFunction)));

	return TRUE;
}

void ChildCrashDlg::OnBnClickedButton1()
{
	if (m_inBgrThread.GetCheck() != BST_CHECKED)
		error_raiser::raise((error_raiser::ExceptionCodeEnum) m_exceptionsCombo.GetItemData(m_exceptionsCombo.GetCurSel()));
	else
		GetThread(0)->Run();
}

void ChildCrashDlg::OnBnClickedButton2()
{
	CString s;
	s.Format(L"Thread ID: %d", GetCurrentThreadId());
	MessageBox(s);
}
