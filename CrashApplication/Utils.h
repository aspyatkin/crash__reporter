#pragma once

// IDestructable
class IDestructable
{
public:
	virtual ~IDestructable();

protected:
	IDestructable();
};

// INullable
class INullable: public IDestructable
{
public:
	virtual bool IsNull() = 0;

protected:
	INullable();
};

// IDelegate0
template <typename TResult = void>
class IDelegate0: public INullable
{
public:
	virtual TResult Invoke() = 0;

protected:
	IDelegate0()
		: INullable()
	{
	}
};

// CDelegate0Chain
template <typename TResult = void>
class CDelegate0Chain: public IDelegate0<TResult>
{
public:
	CDelegate0Chain()
		: IDelegate0<TResult>()
	{
	}

	virtual ~CDelegate0Chain()
	{
		for (int i=0; i<(int) m_delegatePtrArray.size(); ++i)
			delete m_delegatePtrArray[i];
	}

	virtual TResult Invoke()
	{
		for (int i=0; i<(int) m_delegatePtrArray.size(); ++i)
			m_delegatePtrArray[i]->Invoke();
	}

	CDelegate0Chain<TResult> * Add(IDelegate0<TResult> * delegatePtr)
	{
		if (!delegatePtr->IsNull())
			m_delegatePtrArray.push_back(delegatePtr);
		return this;
	}

	bool IsNull()
	{
		return m_delegatePtrArray.empty();
	}
protected:
	std::vector<IDelegate0<TResult> *> m_delegatePtrArray;
};

// CNullDelegate0
template <typename TResult = void>
class CNullDelegate0: public IDelegate0<TResult>
{
public:
	CNullDelegate0()
		: IDelegate0<TResult>()
	{
	}

	virtual TResult Invoke()
	{
	}

	virtual bool IsNull()
	{
		return true;
	}
};

// CFuncDelegate0
template <typename TResult = void>
class CFuncDelegate0: public IDelegate0<TResult>
{
public:
	typedef TResult (*TFunc)();

	CFuncDelegate0(TFunc funcPtr)
		: IDelegate0<TResult>(), m_funcPtr(funcPtr)
	{
	}

	virtual TResult Invoke()
	{
		return (*m_funcPtr)();
	}

	virtual bool IsNull()
	{
		return false;
	}
protected:
	TFunc m_funcPtr;
};

// CMemFuncDelegate0
template <typename TObject, typename TResult = void>
class CMemFuncDelegate0: public IDelegate0<TResult>
{
public:
	typedef TResult (TObject::*TFunc)();

	CMemFuncDelegate0(TObject * objectPtr, TFunc funcPtr)
		: IDelegate0<TResult>(), m_objectPtr(objectPtr), m_funcPtr(funcPtr)
	{
	}

	virtual TResult Invoke()
	{
		return (m_objectPtr->*m_funcPtr)();
	}

	virtual bool IsNull()
	{
		return false;
	}

protected:
	TObject * m_objectPtr;
	TFunc m_funcPtr;
};

// IDelegate1
template <typename TParam1, typename TResult = void>
class IDelegate1: public INullable
{
public:
	virtual TResult Invoke(TParam1) = 0;

protected:
	IDelegate1()
		: INullable()
	{
	}
};

// CNullDelegate1
template <typename TParam1, typename TResult = void>
class CNullDelegate1: public IDelegate1<TParam1, TResult>
{
public:
	CNullDelegate1()
		: IDelegate1<TParam1, TResult>()
	{
	}

	virtual TResult Invoke(TParam1)
	{
	}

	virtual bool IsNull()
	{
		return true;
	}
};

// CFuncDelegate1
template <typename TParam1, typename TResult = void>
class CFuncDelegate1: public IDelegate1<TParam1, TResult>
{
public:
	typedef TResult (*TFunc)(TParam1);

	CFuncDelegate1(TFunc funcPtr)
		: IDelegate1<TParam1, TResult>(), m_funcPtr(funcPtr)
	{
	}

	virtual TResult Invoke(TParam1 param1)
	{
		return (*m_funcPtr)(param1);
	}

	virtual bool IsNull()
	{
		return false;
	}
protected:
	TFunc m_funcPtr;
};

// CMemFuncDelegate1
template <typename TObject, typename TParam1, typename TResult = void>
class CMemFuncDelegate1: public IDelegate1<TParam1, TResult>
{
public:
	typedef TResult (TObject::*TFunc)(TParam1);

	CMemFuncDelegate1(TObject * objectPtr, TFunc funcPtr)
		: IDelegate1<TParam1, TResult>(), m_objectPtr(objectPtr), m_funcPtr(funcPtr)
	{
	}

	virtual TResult Invoke(TParam1 param1)
	{
		return (m_objectPtr->*m_funcPtr)(param1);
	}

	virtual bool IsNull()
	{
		return false;
	}

protected:
	TObject * m_objectPtr;
	TFunc m_funcPtr;
};


// delegates
namespace delegates
{
	template <typename TResult>
	IDelegate0<TResult> * make(TResult (*funcPtr)())
	{
		return new CFuncDelegate0<TResult>(funcPtr);
	}
	
	template <typename TObject, typename TResult>
	IDelegate0<TResult> * make(TObject * objectPtr, TResult (TObject::*funcPtr)())
	{
		return new CMemFuncDelegate0<TObject, TResult>(objectPtr, funcPtr);
	}

	template <typename TParam1, typename TResult>
	IDelegate1<TParam1, TResult> * make(TResult (*funcPtr)(TParam1))
	{
		return new CFuncDelegate1<TParam1, TResult>(funcPtr);
	}
	
	template <typename TObject, typename TParam1, typename TResult>
	IDelegate1<TParam1, TResult> * make(TObject * objectPtr, TResult (TObject::*funcPtr)(TParam1))
	{
		return new CMemFuncDelegate1<TObject, TParam1, TResult>(objectPtr, funcPtr);
	}
};


class IThread;

// ICancelable
class ICancelable: public INullable
{
	friend class IThread;
public:
	virtual void SetCancel(bool cancel) = 0;
	virtual bool IsCancel() const = 0;

protected:
	ICancelable();

	virtual void Refresh() = 0;
};

// CWinApiCancelable
class CWinApiCancelable: public ICancelable
{
public:
	CWinApiCancelable();
	virtual ~CWinApiCancelable();

	virtual void SetCancel(bool cancel);
	virtual bool IsCancel() const;
	virtual bool IsNull();
protected:
	bool m_cancel;
	HANDLE m_accessMutex;

	virtual void Refresh(); 
};

// CNullCancellable
class CNullCancelable: public ICancelable
{
public:
	CNullCancelable();

	virtual void SetCancel(bool cancel);
	virtual bool IsCancel() const;
	virtual bool IsNull();

protected:
	virtual void Refresh();
};

// IReportData
class IReportData
{
public:
	int GetReportCode();
protected:
	int m_reportCode;

	IReportData(int reportCode);
};

// IReportable
class IReportable: public INullable
{
public:
	virtual void Report(IReportData * dataPtr) = 0;

	virtual void Refresh() = 0;
protected:
	IReportable();
};

class CWinApiThread;

// CWinApiReportable
class CWinApiReportable: public IReportable
{
public:
	CWinApiReportable(CWinApiThread * threadPtr, HWND mainWnd, UINT messageNumber = WM_USER + 123);

	virtual void Report(IReportData * dataPtr);
	virtual bool IsNull();

	virtual void Refresh();
protected:
	UINT m_messageNumber;
	HWND m_mainWnd;
	CWinApiThread * m_threadPtr;
};

// CNullReportable
class CNullReportable: public IReportable
{
public:
	CNullReportable();

	virtual void Report(IReportData * dataPtr);
	virtual bool IsNull();

	virtual void Refresh();
};

// IThreadData
class IThreadData
{
public:
	ICancelable * GetCancelable();
	IReportable * GetReportable();

	IThreadData(ICancelable * cancelablePtr, IReportable * reportablePtr);
protected:
	ICancelable * m_cancelablePtr;
	IReportable * m_reportablePtr;
};

class IThread;

// CCancelReport
class CStopReport: public IReportData
{
public:
	static const int REPORT_CODE = 1;

	CStopReport();
};

// CProgressData
class CProgressData: public IReportData
{
public:
	static const int REPORT_CODE = 2;

	CProgressData(int currentProgress);

	int GetCurrentProgress() const;
protected:
	int m_currentProgress;
};

// CProgressReporter
class CProgressReporter
{
public:
	CProgressReporter(IReportable * reportablePtr, int minVal = 0, int maxVal = 100, int addition = 1);

	void Progress();
	void Report();
protected:
	IReportable * m_reportablePtr;
	int m_minVal;
	int m_maxVal;
	int m_curVal;
	int m_addition;
};

// IThread
class IThread: public IDestructable
{
public:
	enum ThreadStateEnum
	{
		UndefinedState,
		RunningState,
		PausedState,
		StoppingState
	};

	typedef IThreadData * TData;

	IThread * SetOnStarted(IDelegate0<> * onStarted);
	IThread * SetOnStopped(IDelegate0<> * onStopped);
	IThread * SetOnPaused(IDelegate0<> * onPaused);
	IThread * SetOnResumed(IDelegate0<> * onResumed);
	IThread * SetOnProgress(IDelegate1<const CProgressData &> * onProgress);

	void Run();
	void Pause();
	void Stop();
	void StopAsync();

	ThreadStateEnum GetState() const;

	virtual void Refresh() = 0;
protected:
	IThread(IDelegate1<TData> * threadFunc);

	virtual void DoRun() = 0;
	virtual void DoPause() = 0;
	virtual void DoStop() = 0;
	virtual void DoStopAsync() = 0;

	std::auto_ptr<IDelegate0<>> m_onStarted;
	std::auto_ptr<IDelegate0<>> m_onPaused;
	std::auto_ptr<IDelegate0<>> m_onResumed;
	std::auto_ptr<IDelegate0<>> m_onStopped;
	std::auto_ptr<IDelegate1<const CProgressData &>> m_onProgress;
	std::auto_ptr<IDelegate1<TData>> m_threadFunc;

	friend class CThreadContainer;
	friend class IState;

	class IState: public IDestructable
	{
	public:
		virtual bool Reset() = 0;
		virtual bool Run() = 0;
		virtual bool Pause() = 0;
		virtual bool Stop() = 0;

		virtual ThreadStateEnum GetStateCode() const = 0;
	protected:
		IState(IThread * threadPtr);

		IThread * m_threadPtr;
	};

	std::auto_ptr<IState> m_threadState;

	class CNullState: public IState
	{
	public:
		CNullState(IThread * threadPtr);
		
		virtual bool Reset();
		virtual bool Run();
		virtual bool Pause();
		virtual bool Stop();

		virtual ThreadStateEnum GetStateCode() const;
	};

	class CUndefinedState: public CNullState
	{
	public:
		CUndefinedState(IThread * threadPtr);

		virtual bool Run();
	};

	class CRunningState: public CNullState
	{
	public:
		CRunningState(IThread * threadPtr);

		virtual bool Pause();
		virtual bool Stop();
		
		virtual ThreadStateEnum GetStateCode() const;
	};

	class CPausedState: public CNullState
	{
	public:
		CPausedState(IThread * threadPtr);

		virtual bool Run();
		virtual bool Stop();

		virtual ThreadStateEnum GetStateCode() const;
	};

	class CStoppingState: public CNullState
	{
	public:
		CStoppingState(IThread * threadPtr);

		virtual bool Reset();

		virtual ThreadStateEnum GetStateCode() const;
	};

	void Refresh(ICancelable * cancelablePtr, IReportable * reportablePtr);
private:
	void OnThreadStopped();
};

// CWinApiThread
class CWinApiThread: public IThread
{
public:
	CWinApiThread(HWND mainWnd, UINT messageNum, IDelegate1<IThread::TData> * threadFunc);

	virtual void Refresh();
protected:
	virtual void DoRun();
	virtual void DoPause();
	virtual void DoStop();
	virtual void DoStopAsync();

	void Dispose();

	DWORD m_threadId;
	HANDLE m_threadHandle;
	std::auto_ptr<CWinApiCancelable> m_cancelablePtr;
	std::auto_ptr<CWinApiReportable> m_reportablePtr;

	std::pair<CWinApiThread *, std::auto_ptr<IThreadData>> m_threadRawData;

	static DWORD WINAPI ThreadFunc(void * dataPtr);
};

// CThreadContainer
class CThreadContainer: public IDestructable
{
public:
	void AddThread(IThread * threadPtr);

	IThread * GetThread(int n);
protected:
	CThreadContainer();

	LRESULT OnThreadSentData(WPARAM wParam, LPARAM lParam);
private:
	std::vector<std::unique_ptr<IThread>> m_threadPtrArray;
};