#pragma once

namespace error_raiser
{
	enum ExceptionCodeEnum
	{
		CrtTerminateException,
		CrtPurecallException,
		StructuredException,
		CrtUnexpectedException,
		CrtInvalidParameterException,
		CrtNewException,
		DivideZeroException
	};


	void raise(ExceptionCodeEnum code);

	void fillCombo(CComboBox * pCombo);
}