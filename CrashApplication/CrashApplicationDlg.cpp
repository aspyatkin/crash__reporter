
// CrashApplicationDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CrashApplication.h"
#include "CrashApplicationDlg.h"
#include "afxdialogex.h"
#include "ChildCrashDlg.h"
#include "ErrorRaiser.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CCrashApplicationDlg dialog



CCrashApplicationDlg::CCrashApplicationDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CCrashApplicationDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CCrashApplicationDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO1, m_exceptionsCombo);
}

BEGIN_MESSAGE_MAP(CCrashApplicationDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDOK, &CCrashApplicationDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDCANCEL, &CCrashApplicationDlg::OnBnClickedCancel)
	ON_CBN_SELCHANGE(IDC_COMBO1, &CCrashApplicationDlg::OnCbnSelchangeCombo1)
	ON_BN_CLICKED(IDC_BUTTON2, &CCrashApplicationDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON1, &CCrashApplicationDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON3, &CCrashApplicationDlg::OnBnClickedButton3)
END_MESSAGE_MAP()


// CCrashApplicationDlg message handlers

BOOL CCrashApplicationDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	// TODO: Add extra initialization here
	error_raiser::fillCombo(&m_exceptionsCombo);
	m_exceptionsCombo.SetCurSel(0);


	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CCrashApplicationDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// The system calls this function to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CCrashApplicationDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CCrashApplicationDlg::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	CDialogEx::OnOK();
}


void CCrashApplicationDlg::OnBnClickedCancel()
{
	// TODO: Add your control notification handler code here
	CDialogEx::OnCancel();
}


void CCrashApplicationDlg::OnCbnSelchangeCombo1()
{
	// TODO: Add your control notification handler code here
}


void CCrashApplicationDlg::OnBnClickedButton2()
{
	error_raiser::raise((error_raiser::ExceptionCodeEnum) m_exceptionsCombo.GetItemData(m_exceptionsCombo.GetCurSel()));
}


void CCrashApplicationDlg::OnBnClickedButton1()
{
	// TODO: Add your control notification handler code here
	ChildCrashDlg dlg;
	dlg.DoModal();
}


void CCrashApplicationDlg::OnBnClickedButton3()
{
	CString s;
	s.Format(L"Current Thread ID: %d", GetCurrentThreadId());
	MessageBox(s);
}
