#pragma once
#include "afxwin.h"
#include "Utils.h"

// ChildCrashDlg dialog

class ChildCrashDlg : public CDialog, CThreadContainer
{
	LRESULT OnThreadSentData(WPARAM, LPARAM);
	void CrashThreadFunction(IThread::TData);

	DECLARE_DYNAMIC(ChildCrashDlg)
public:
	ChildCrashDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~ChildCrashDlg();

	virtual BOOL OnInitDialog();
// Dialog Data
	enum { IDD = IDD_CHILDCRASHDLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CComboBox m_exceptionsCombo;
	afx_msg void OnBnClickedButton1();
	CButton m_inBgrThread;
	afx_msg void OnBnClickedButton2();
};
