#pragma once
#include <memory>
#include <vector>

namespace global_error_handling
{
	namespace Local
	{
		template <typename T>
		class ISingleton
		{
		public:
			virtual ~ISingleton()
			{
			}

			static T & instance()
			{
				if (!m_instancePtr)
					m_instancePtr = new T();
				return *m_instancePtr;
			}

		private:
			static T * m_instancePtr;

			static void releaseInstance()
			{
				if (m_instancePtr)
					delete m_instancePtr;
			}

		protected:
			ISingleton()
			{
				atexit(releaseInstance);
			}

		};

		template <typename T>
		T * ISingleton<T>::m_instancePtr = 0;


		class CExceptionInfo
		{
		public:
			CExceptionInfo(const CString & message);

			virtual ~CExceptionInfo();
			virtual CString getDescription() const;
		protected:
			CString m_message;
		};


		class IExceptionHandler
		{
		public:
			virtual ~IExceptionHandler()
			{
			}

			virtual void activateHandler() = 0;

			virtual BOOL isForProcess() const = 0;
			virtual BOOL isForThread() const = 0;
		protected:
			IExceptionHandler();
		};


		class CExceptionReporter: public ISingleton<CExceptionReporter>
		{
			std::vector<std::unique_ptr<IExceptionHandler>> m_handlers;

			static const CString m_sPipeName;
		public:
			CExceptionReporter();
			virtual ~CExceptionReporter();

			void reportException(const CExceptionInfo & infoPtr) const;

			void activateForProcess();
			void activateForThread();
		};


		class CCrtTerminateExceptionHandler: public IExceptionHandler
		{
			static void CrtTerminateHandler();
		public:
			CCrtTerminateExceptionHandler();

			virtual void activateHandler();

			virtual BOOL isForProcess() const;
			virtual BOOL isForThread() const;
		};


		class CCrtUnexpectedExceptionHandler: public IExceptionHandler
		{
			static void CrtUnexpectedHandler();
		public:
			CCrtUnexpectedExceptionHandler();

			virtual void activateHandler();

			virtual BOOL isForProcess() const;
			virtual BOOL isForThread() const;
		};


		class CCrtPureCallExceptionHandler: public IExceptionHandler
		{
			static void CrtPureCallHandler();
		public:
			CCrtPureCallExceptionHandler();

			virtual void activateHandler();

			virtual BOOL isForProcess() const;
			virtual BOOL isForThread() const;
		};


		class CStructuredExceptionHandler: public IExceptionHandler
		{
			static LONG WINAPI StructuredExceptionFilter(PEXCEPTION_POINTERS pExceptions);
		public:
			CStructuredExceptionHandler();

			virtual void activateHandler();

			virtual BOOL isForProcess() const;
			virtual BOOL isForThread() const;
		};
	

		class CCrtInvalidParameterExceptionHandler: public IExceptionHandler
		{
			static void InvalidParameterHandler(const wchar_t * expression, const wchar_t * function, const wchar_t * file, unsigned int line, uintptr_t pReserved);
		public:
			CCrtInvalidParameterExceptionHandler();

			virtual void activateHandler();

			virtual BOOL isForProcess() const;
			virtual BOOL isForThread() const;
		};


		class CCrtNewExceptionHandler: public IExceptionHandler
		{
			static int NewExceptionHandler(size_t);
		public:
			CCrtNewExceptionHandler();

			virtual void activateHandler();

			virtual BOOL isForProcess() const;
			virtual BOOL isForThread() const;
		};
	};

	void activateForProcess();
	void activateForThread();
};