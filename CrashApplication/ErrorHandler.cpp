#include "stdafx.h"
#include "ErrorHandler.h"

namespace global_error_handling
{
	namespace Local
	{
		//
		// CExceptionInfo class
		//
		CExceptionInfo::CExceptionInfo(const CString & message)
			: m_message(message)
		{
		}

		CExceptionInfo::~CExceptionInfo()
		{
		}

		CString CExceptionInfo::getDescription() const
		{
			return m_message;
		}


		//
		// IExceptionHandler class
		//
		IExceptionHandler::IExceptionHandler()
		{
		}


		//
		// CExceptionReporter class
		//
		const CString CExceptionReporter::m_sPipeName = L"\\\\.\\pipe\\mynamedpipe";

		CExceptionReporter::CExceptionReporter()
			: ISingleton<CExceptionReporter>()
		{
			m_handlers.push_back(std::unique_ptr<IExceptionHandler>(new CStructuredExceptionHandler()));
			m_handlers.push_back(std::unique_ptr<IExceptionHandler>(new CCrtTerminateExceptionHandler()));
			m_handlers.push_back(std::unique_ptr<IExceptionHandler>(new CCrtUnexpectedExceptionHandler()));
			m_handlers.push_back(std::unique_ptr<IExceptionHandler>(new CCrtPureCallExceptionHandler()));
			m_handlers.push_back(std::unique_ptr<IExceptionHandler>(new CCrtInvalidParameterExceptionHandler()));
			m_handlers.push_back(std::unique_ptr<IExceptionHandler>(new CCrtNewExceptionHandler()));
		}

		CExceptionReporter::~CExceptionReporter()
		{
			m_handlers.clear();
		}

		void CExceptionReporter::reportException(const CExceptionInfo & infoPtr) const
		{
			const int nBufferLen = 65536;
			HANDLE hPipe;
		

			TCHAR buf [nBufferLen];
			ZeroMemory(buf, nBufferLen * sizeof(TCHAR));
			DWORD dw = GetModuleFileName(NULL, buf, nBufferLen);
			buf[dw] = L'\0';
			CString sMessage = CString(buf) + L"\r\n" + infoPtr.getDescription();
			CString sThreadID;
			sThreadID.Format(L"%d", GetCurrentThreadId());
			sMessage += L"\r\nThread ID " + sThreadID; 

			while (true)
			{ 
				hPipe = CreateFile(m_sPipeName, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, 0, NULL);
 
				if (hPipe != INVALID_HANDLE_VALUE) 
					break; 
 
				if (GetLastError() != ERROR_PIPE_BUSY) 
				{
					_tprintf( TEXT("Could not open pipe. GLE=%d\n"), GetLastError() ); 
					return;
				}
 
				if (!WaitNamedPipe(m_sPipeName, 20000)) 
				{ 
					printf("Could not open pipe: 20 second wait timed out."); 
					return;
				} 
			} 
 
			DWORD dwMode = PIPE_READMODE_MESSAGE; 
			BOOL bSuccess = SetNamedPipeHandleState(hPipe, &dwMode, NULL, NULL); 
			if (!bSuccess)
			{
				_tprintf( TEXT("SetNamedPipeHandleState failed. GLE=%d\n"), GetLastError() ); 
				return;
			}
 
			TCHAR bufOut[nBufferLen];
			ZeroMemory(bufOut, nBufferLen * sizeof(TCHAR));
			for (int i=0; i<sMessage.GetLength(); ++i)
				bufOut[i] = sMessage[i];
			bufOut[sMessage.GetLength()] = L'\0';
			DWORD dwToWrite = (sMessage.GetLength() + 1) * sizeof(TCHAR);

			DWORD dwWritten = 0;
			bSuccess = WriteFile(hPipe, bufOut, dwToWrite, &dwWritten, NULL);

			if (!bSuccess) 
			{
				_tprintf( TEXT("WriteFile to pipe failed. GLE=%d\n"), GetLastError() ); 
				return;
			}

			CloseHandle(hPipe); 
		}

		void CExceptionReporter::activateForProcess()
		{
			for (int i=0; i<(int) m_handlers.size(); ++i)
				if (m_handlers[i]->isForProcess())
					m_handlers[i]->activateHandler();
		}

		void CExceptionReporter::activateForThread()
		{
			for (int i=0; i<(int) m_handlers.size(); ++i)
				if (m_handlers[i]->isForThread())
					m_handlers[i]->activateHandler();
		}


		//
		// CCrtTerminateExceptionHandler class
		//
		CCrtTerminateExceptionHandler::CCrtTerminateExceptionHandler()
			: IExceptionHandler()
		{
		}

		void CCrtTerminateExceptionHandler::activateHandler()
		{
			set_terminate(CrtTerminateHandler);
		}

		BOOL CCrtTerminateExceptionHandler::isForProcess() const
		{
			return TRUE;
		}

		BOOL CCrtTerminateExceptionHandler::isForThread() const
		{
			return TRUE;
		}

		void CCrtTerminateExceptionHandler::CrtTerminateHandler()
		{
			CExceptionReporter::instance().reportException(CExceptionInfo(L"crt terminate() exception"));
			exit(1);
		}


		//
		// CCrtUnexpectedExceptionHandler class
		//
		CCrtUnexpectedExceptionHandler::CCrtUnexpectedExceptionHandler()
			: IExceptionHandler()
		{
		}

		void CCrtUnexpectedExceptionHandler::activateHandler()
		{
			set_unexpected(CrtUnexpectedHandler);
		}

		BOOL CCrtUnexpectedExceptionHandler::isForProcess() const
		{
			return TRUE;
		}

		BOOL CCrtUnexpectedExceptionHandler::isForThread() const
		{
			return TRUE;
		}

		void CCrtUnexpectedExceptionHandler::CrtUnexpectedHandler()
		{
			CExceptionReporter::instance().reportException(CExceptionInfo(L"crt unexpected exception"));
			exit(1);
		}


		//
		// CCrtPureCallExceptionHandler class
		//
		CCrtPureCallExceptionHandler::CCrtPureCallExceptionHandler()
			: IExceptionHandler()
		{
		}

		void CCrtPureCallExceptionHandler::activateHandler()
		{
			_set_purecall_handler(CrtPureCallHandler);
		}

		BOOL CCrtPureCallExceptionHandler::isForProcess() const
		{
			return TRUE;
		}

		BOOL CCrtPureCallExceptionHandler::isForThread() const
		{
			return FALSE;
		}

		void CCrtPureCallExceptionHandler::CrtPureCallHandler()
		{
			CExceptionReporter::instance().reportException(CExceptionInfo(L"crt purecall exception"));
			exit(1);
		}


		//
		// CStructuredExceptionHandler class
		//
		CStructuredExceptionHandler::CStructuredExceptionHandler()
			: IExceptionHandler()
		{
		}

		void CStructuredExceptionHandler::activateHandler()
		{
			SetUnhandledExceptionFilter(StructuredExceptionFilter);
		}

		BOOL CStructuredExceptionHandler::isForProcess() const
		{
			return TRUE;
		}

		BOOL CStructuredExceptionHandler::isForThread() const
		{
			return FALSE;
		}

		LONG WINAPI CStructuredExceptionHandler::StructuredExceptionFilter(PEXCEPTION_POINTERS pExceptions)
		{
			CExceptionReporter::instance().reportException(CExceptionInfo(L"SEH exception"));
			return EXCEPTION_EXECUTE_HANDLER;
		}


		//
		// CCrtInvalidParameterExceptionHandler class
		//
		CCrtInvalidParameterExceptionHandler::CCrtInvalidParameterExceptionHandler()
			: IExceptionHandler()
		{
		}

		void CCrtInvalidParameterExceptionHandler::activateHandler()
		{
			_set_invalid_parameter_handler(InvalidParameterHandler);
		}

		BOOL CCrtInvalidParameterExceptionHandler::isForProcess() const
		{
			return TRUE;
		}

		BOOL CCrtInvalidParameterExceptionHandler::isForThread() const
		{
			return FALSE;
		}

		void CCrtInvalidParameterExceptionHandler::InvalidParameterHandler(const wchar_t * expression, const wchar_t * function, const wchar_t * file, 
			unsigned int line, uintptr_t pReserved)
		{
			CString sMessage;
			sMessage.Format(L"Invalid parameter detected in function %s.\r\nFile %s, line %d.\r\nExpression: %s.",
				function, file, line, expression);
			CExceptionReporter::instance().reportException(CExceptionInfo(sMessage));
		}


		//
		// CCrtNewExceptionHandler class
		//
		CCrtNewExceptionHandler::CCrtNewExceptionHandler()
			: IExceptionHandler()
		{
		}

		void CCrtNewExceptionHandler::activateHandler()
		{
			_set_new_handler(NewExceptionHandler);
		}

		BOOL CCrtNewExceptionHandler::isForProcess() const
		{
			return TRUE;
		}

		BOOL CCrtNewExceptionHandler::isForThread() const
		{
			return FALSE;
		}

		int CCrtNewExceptionHandler::NewExceptionHandler(size_t n)
		{
			CString s;
			s.Format(L"operator new(size_t) error while allocating %d bytes.", n);
			CExceptionReporter::instance().reportException(CExceptionInfo(s));
			exit(1);
			return 0;
		}
	}



	void activateForProcess()
	{
		Local::CExceptionReporter::instance().activateForProcess();
	}

	void activateForThread()
	{
		Local::CExceptionReporter::instance().activateForThread();
	}
}