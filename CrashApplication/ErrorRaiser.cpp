#include "stdafx.h"
#include "ErrorRaiser.h"
#include <eh.h>

namespace error_raiser
{
	class CDerived;
	class CBase
	{
	public:
	   CBase(CDerived *derived): m_pDerived(derived) {};
	   ~CBase();
	   virtual void function(void) = 0;
	   CDerived * m_pDerived;
	};
	class CDerived : public CBase
	{
	public:
	   CDerived() : CBase(this) {};   // C4355
	   virtual void function(void) {};
	};
	CBase::~CBase()
	{
	   m_pDerived -> function();
	}

	#define BIG_NUMBER 0x1fffffff
	#pragma warning(disable: 4717) // avoid C4717 warning
	int RecurseAlloc() 
	{
		int *pi = new int[BIG_NUMBER];
		pi;
		RecurseAlloc();
		return 0;
	}

	void raise(ExceptionCodeEnum code)
	{
		switch (code)
		{
		case ExceptionCodeEnum::CrtTerminateException:
			{
				terminate();
			}
			break;
		case ExceptionCodeEnum::StructuredException:
			{
				RaiseException(123, EXCEPTION_NONCONTINUABLE, 0, NULL);
			}
			break;
		case ExceptionCodeEnum::CrtPurecallException:
			{
				CDerived derived;
			}
			break;
		case ExceptionCodeEnum::CrtUnexpectedException:
			{
				unexpected();
			}
			break;
		case ExceptionCodeEnum::CrtInvalidParameterException:
			{
				TCHAR * pStr = NULL;
				_tprintf(pStr);
			}
			break;
		case ExceptionCodeEnum::CrtNewException:
			{
				RecurseAlloc();
			}
			break;
		case ExceptionCodeEnum::DivideZeroException:
			{
				float f1 = 45.78;
				float f2 = 0.0;
				float r = f1/f2;
				CString s;
				s.Format(L"%f/%f = %f", f1, f2, r);
			}
			break;
		default:
			break;
		};
	}

	void fillCombo(CComboBox * pCombo)
	{
		pCombo->InsertString(0, L"CRT terminate");
		pCombo->SetItemData(0, ExceptionCodeEnum::CrtTerminateException);

		pCombo->InsertString(1, L"CRT purecall");
		pCombo->SetItemData(1, ExceptionCodeEnum::CrtPurecallException);

		pCombo->InsertString(2, L"SEH");
		pCombo->SetItemData(2, ExceptionCodeEnum::StructuredException);

		pCombo->InsertString(3, L"CRT unexpected exception");
		pCombo->SetItemData(3, ExceptionCodeEnum::CrtUnexpectedException);

		pCombo->InsertString(4, L"CRT invalid parameter exception");
		pCombo->SetItemData(4, ExceptionCodeEnum::CrtInvalidParameterException);

		pCombo->InsertString(5, L"CRT new exception");
		pCombo->SetItemData(5, ExceptionCodeEnum::CrtNewException);

		pCombo->InsertString(6, L"Divide zero exception");
		pCombo->SetItemData(6, ExceptionCodeEnum::DivideZeroException);

		//pCombo->InsertString(, L"");
		//pCombo->SetItemData(, ExceptionCodeEnum::);
	}
}