#include "stdafx.h"
#include "Utils.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// IDestructable
IDestructable::IDestructable()
{
}

IDestructable::~IDestructable()
{
}

// INullable
INullable::INullable()
	: IDestructable()
{
}

// ICancelable
ICancelable::ICancelable()
	: INullable()
{
}

// CWinApiCancelable
CWinApiCancelable::CWinApiCancelable()
	: ICancelable(), m_cancel(false)
{
	m_accessMutex = CreateMutex(NULL, FALSE, NULL);
	if (m_accessMutex == NULL)
		throw NULL;
}

CWinApiCancelable::~CWinApiCancelable()
{
	CloseHandle(m_accessMutex);
}

bool CWinApiCancelable::IsNull()
{
	return false;
}

void CWinApiCancelable::SetCancel(bool cancel)
{
	if (WaitForSingleObject(m_accessMutex, INFINITE) == WAIT_OBJECT_0)
	{
		m_cancel = cancel;

		if (!ReleaseMutex(m_accessMutex))
			throw NULL;
	}
	else
	{
		throw NULL;
	}
}

bool CWinApiCancelable::IsCancel() const
{
	if (WaitForSingleObject(m_accessMutex, INFINITE) == WAIT_OBJECT_0)
	{
		bool result = m_cancel;

		if (!ReleaseMutex(m_accessMutex))
			throw NULL;

		return result;
	}

	throw NULL;
}

void CWinApiCancelable::Refresh()
{
	m_cancel = false;
	CloseHandle(m_accessMutex);
	m_accessMutex = CreateMutex(NULL, FALSE, NULL);
	if (m_accessMutex == NULL)
		throw NULL;
}

// CNullCancelable
CNullCancelable::CNullCancelable()
	: ICancelable()
{
}

void CNullCancelable::SetCancel(bool cancel)
{
}

bool CNullCancelable::IsCancel() const
{
	return false;
}

bool CNullCancelable::IsNull()
{
	return true;
}

void CNullCancelable::Refresh()
{
}

// IReportData
IReportData::IReportData(int reportCode)
	: m_reportCode(reportCode)
{
}

int IReportData::GetReportCode()
{
	return m_reportCode;
}

// IReportable
IReportable::IReportable()
	: INullable()
{
}

// CWinApiReportable
CWinApiReportable::CWinApiReportable(CWinApiThread * threadPtr, HWND mainWnd, UINT messageNumber)
	: IReportable(), m_threadPtr(threadPtr), m_mainWnd(mainWnd), m_messageNumber(messageNumber)
{
}

void CWinApiReportable::Report(IReportData * dataPtr)
{
	std::pair<CWinApiThread *, IReportData *> * pairPtr = new std::pair<CWinApiThread *, IReportData *>();
	pairPtr->first = m_threadPtr;
	pairPtr->second = dataPtr;
	PostMessage(m_mainWnd, m_messageNumber, reinterpret_cast<WPARAM> (pairPtr), 0);
}

bool CWinApiReportable::IsNull()
{
	return false;
}

void CWinApiReportable::Refresh()
{
}

// CNullReportable
CNullReportable::CNullReportable()
	: IReportable()
{
}

void CNullReportable::Report(IReportData * dataPtr)
{
}

bool CNullReportable::IsNull()
{
	return true;
}

void CNullReportable::Refresh()
{
}

// IThreadData
IThreadData::IThreadData(ICancelable * cancelablePtr, IReportable * reportablePtr)
	: m_cancelablePtr(cancelablePtr), m_reportablePtr(reportablePtr)
{
}

ICancelable * IThreadData::GetCancelable()
{
	return m_cancelablePtr;
}

IReportable * IThreadData::GetReportable()
{
	return m_reportablePtr;
}

// CCancelReport
CStopReport::CStopReport()
	: IReportData(REPORT_CODE)
{
}

// CProgressData
CProgressData::CProgressData(int currentProgress)
	: IReportData(REPORT_CODE), m_currentProgress(currentProgress)
{
}

int CProgressData::GetCurrentProgress() const
{
	return m_currentProgress;
}

// CProgressReporter
CProgressReporter::CProgressReporter(IReportable * reportablePtr, int minVal, int maxVal, int addition)
	: m_reportablePtr(reportablePtr), m_minVal(minVal), m_maxVal(maxVal), m_curVal(m_minVal), m_addition(addition)
{
}

void CProgressReporter::Progress()
{
	m_curVal += m_addition;

	if (m_curVal > m_maxVal)
		m_curVal = m_maxVal;
}

void CProgressReporter::Report()
{
	m_reportablePtr->Report(new CProgressData(m_curVal));
}

// IThread
IThread::IThread(IDelegate1<IThread::TData> * threadFunc)
	: IDestructable(), m_onStarted(new CNullDelegate0<>()), m_onProgress(new CNullDelegate1<const CProgressData &>()),
	m_threadFunc(threadFunc), m_threadState(new CUndefinedState(this)), m_onStopped(delegates::make(this, &IThread::OnThreadStopped)),
	m_onPaused(new CNullDelegate0<>()), m_onResumed(new CNullDelegate0<>())
{
}

void IThread::Run()
{
	bool invokeOnStarted = (m_threadState->GetStateCode() == IThread::UndefinedState);

	if (m_threadState->Run())
	{
		DoRun();
		invokeOnStarted ? m_onStarted->Invoke() : m_onResumed->Invoke();
	}
}

void IThread::Pause()
{
	if (m_threadState->Pause())
	{
		DoPause();
		m_onPaused->Invoke();
	}
}

void IThread::Stop()
{
	if (m_threadState->Stop())
		DoStop();
}

void IThread::StopAsync()
{
	if (m_threadState->Stop())
		DoStopAsync();
}

IThread * IThread::SetOnStarted(IDelegate0<> * onStarted)
{
	m_onStarted.reset(onStarted);
	return this;
}

IThread * IThread::SetOnStopped(IDelegate0<> * onStopped)
{
	IDelegate0<> * delegatePtr = (new CDelegate0Chain<>())->Add(delegates::make(this, &IThread::OnThreadStopped))->Add(onStopped);
	m_onStopped.reset(delegatePtr);	
	return this;
}

IThread * IThread::SetOnProgress(IDelegate1<const CProgressData &> * onProgress)
{
	m_onProgress.reset(onProgress);
	return this;
}

IThread * IThread::SetOnPaused(IDelegate0<> * onPaused)
{
	m_onPaused.reset(onPaused);
	return this;
}

IThread * IThread::SetOnResumed(IDelegate0<> * onResumed)
{
	m_onResumed.reset(onResumed);
	return this;
}

void IThread::Refresh(ICancelable * cancelablePtr, IReportable * reportablePtr)
{
	cancelablePtr->Refresh();
	reportablePtr->Refresh();
}

void IThread::OnThreadStopped()
{
	m_threadState->Reset();
}

IThread::ThreadStateEnum IThread::GetState() const
{
	return m_threadState->GetStateCode();
}

// IThread::IState
IThread::IState::IState(IThread * threadPtr)
	: IDestructable(), m_threadPtr(threadPtr)
{
}

// IThread::CNullState
IThread::CNullState::CNullState(IThread * threadPtr)
	: IState(threadPtr)
{
}

bool IThread::CNullState::Run()
{
	return false;
}

bool IThread::CNullState::Pause()
{
	return false;
}

bool IThread::CNullState::Stop()
{
	return false;
}

bool IThread::CNullState::Reset()
{
	return false;
}

IThread::ThreadStateEnum IThread::CNullState::GetStateCode() const
{
	return IThread::UndefinedState;
}

// IThread::CUndefinedState
IThread::CUndefinedState::CUndefinedState(IThread * threadPtr)
	: IThread::CNullState(threadPtr)
{
}

bool IThread::CUndefinedState::Run()
{
	m_threadPtr->m_threadState.reset(new CRunningState(m_threadPtr));
	return true;
}

// IThread::CRunningState
IThread::CRunningState::CRunningState(IThread * threadPtr)
	: IThread::CNullState(threadPtr)
{
}

bool IThread::CRunningState::Pause()
{
	m_threadPtr->m_threadState.reset(new CPausedState(m_threadPtr));
	return true;
}

bool IThread::CRunningState::Stop()
{
	m_threadPtr->m_threadState.reset(new CStoppingState(m_threadPtr));
	return true;
}

IThread::ThreadStateEnum IThread::CRunningState::GetStateCode() const
{
	return IThread::RunningState;
}

// IThread::CPausedState
IThread::CPausedState::CPausedState(IThread * threadPtr)
	: IThread::CNullState(threadPtr)
{
}

bool IThread::CPausedState::Run()
{
	m_threadPtr->m_threadState.reset(new CRunningState(m_threadPtr));
	return true;
}

bool IThread::CPausedState::Stop()
{
	m_threadPtr->m_threadState.reset(new CStoppingState(m_threadPtr));
	return true;
}

IThread::ThreadStateEnum IThread::CPausedState::GetStateCode() const
{
	return IThread::PausedState;
}

// IThread::CStoppingState
IThread::CStoppingState::CStoppingState(IThread * threadPtr)
	: IThread::CNullState(threadPtr)
{
}

bool IThread::CStoppingState::Reset()
{
	m_threadPtr->m_threadState.reset(new CUndefinedState(m_threadPtr));
	return true;
}

IThread::ThreadStateEnum IThread::CStoppingState::GetStateCode() const
{
	return IThread::StoppingState;
}

// CWinApiThread
CWinApiThread::CWinApiThread(HWND mainWnd, UINT messageNum, IDelegate1<IThread::TData> * threadFunc)
	: IThread(threadFunc)
{
	m_cancelablePtr.reset(new CWinApiCancelable());
	m_reportablePtr.reset(new CWinApiReportable(this, mainWnd, messageNum));
	m_threadRawData.first = this;
	m_threadRawData.second.reset(new IThreadData(m_cancelablePtr.get(), m_reportablePtr.get()));

	m_threadHandle = CreateThread(NULL, 0, ThreadFunc, &m_threadRawData, CREATE_SUSPENDED, &m_threadId);
	if (m_threadHandle == NULL)
		throw NULL;
}

void CWinApiThread::Refresh()
{
	if (GetState() != IThread::UndefinedState)
		return;

	__super::Refresh(m_cancelablePtr.get(), m_reportablePtr.get());
	Dispose();
	m_threadHandle = CreateThread(NULL, 0, ThreadFunc, &m_threadRawData, CREATE_SUSPENDED, &m_threadId);
	if (m_threadHandle == NULL)
		throw NULL;
}

void CWinApiThread::DoRun()
{
	if (ResumeThread(m_threadHandle) == -1)
		throw NULL;
}

void CWinApiThread::DoPause()
{
	if (SuspendThread(m_threadHandle) == -1)
		throw NULL;
}

void CWinApiThread::Dispose()
{
	CloseHandle(m_threadHandle);
	m_threadHandle = NULL;
	m_threadId = 0;
}

void CWinApiThread::DoStop()
{
	m_cancelablePtr->SetCancel(true);

	DWORD result = WaitForSingleObject(m_threadHandle, INFINITE);
	if (result == WAIT_OBJECT_0)
		Dispose();
}

void CWinApiThread::DoStopAsync()
{
	m_cancelablePtr->SetCancel(true);
}

DWORD WINAPI CWinApiThread::ThreadFunc(void * dataPtr)
{
	std::pair<CWinApiThread *, std::auto_ptr<IThreadData>> * pairPtr =
		static_cast<std::pair<CWinApiThread *, std::auto_ptr<IThreadData>> *> (dataPtr);

	pairPtr->first->m_threadFunc->Invoke(pairPtr->second.get());
	pairPtr->second->GetReportable()->Report(new CStopReport());
	return 0;
}

// CThreadContainer
CThreadContainer::CThreadContainer()
	: IDestructable()
{
}

void CThreadContainer::AddThread(IThread * threadPtr)
{
	m_threadPtrArray.push_back(std::unique_ptr<IThread>(threadPtr));
}

IThread * CThreadContainer::GetThread(int n)
{
	return (0 <= n && n < (int)m_threadPtrArray.size()) ? m_threadPtrArray[n].get() : NULL;
}

LRESULT CThreadContainer::OnThreadSentData(WPARAM wParam, LPARAM lParam)
{
	std::auto_ptr<std::pair<CWinApiThread *, IReportData *>> pairPtr(reinterpret_cast<std::pair<CWinApiThread *, IReportData *> *> (wParam));
	std::auto_ptr<IReportData> reportPtr(pairPtr->second);

	switch (reportPtr->GetReportCode())
	{
		case CStopReport::REPORT_CODE:
		{
			pairPtr->first->m_onStopped->Invoke();
			break;
		}

		case CProgressData::REPORT_CODE:
		{
			pairPtr->first->m_onProgress->Invoke(* static_cast<CProgressData *> (reportPtr.get()));
			break;
		}
	}

	return 0;
}