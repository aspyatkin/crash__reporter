
// CrashReporterServerGuiDlg.h : header file
//

#pragma once
#include "afxwin.h"
#include "Utils.h"


// CCrashReporterServerGuiDlg dialog
class CCrashReporterServerGuiDlg : public CDialogEx, CThreadContainer
{
// Construction
public:
	CCrashReporterServerGuiDlg(CWnd* pParent = NULL);	// standard constructor
	
	void LogString(const CString &);
// Dialog Data
	enum { IDD = IDD_CRASHREPORTERSERVERGUI_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
private:
	CButton m_actionButton;
	CEdit m_logEdit;

	afx_msg void OnClickedButtonClose();
	afx_msg void OnClickedButtonAction();

	void PipeServerThread(IThread::TData dataPtr);
	void OnPipeServerThreadStarted();
	void OnPipeServerThreadStopped();

	LRESULT OnThreadSentData(WPARAM, LPARAM);

	BOOL m_bReadyToExit;
};
