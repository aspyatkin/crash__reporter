
// CrashReporterServerGuiDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CrashReporterServerGui.h"
#include "CrashReporterServerGuiDlg.h"
#include "afxdialogex.h"
#include <strsafe.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

struct CInstanceThreadData
{
	HANDLE hPipe;
	CCrashReporterServerGuiDlg * m_pDlg;
};

DWORD WINAPI InstanceThread(LPVOID lpvParam)
// This routine is a thread processing function to read from and reply to a client
// via the open pipe connection passed from the main loop. Note this allows
// the main loop to continue executing, potentially creating more threads of
// of this procedure to run concurrently, depending on the number of incoming
// client connections.
{
	const int nBufferLen = 65536;
	CInstanceThreadData data = *(static_cast<CInstanceThreadData *> (lpvParam));

	HANDLE hHeap = GetProcessHeap();
	HANDLE hPipe  = data.hPipe;
	TCHAR * pBuffer = (TCHAR*) HeapAlloc(hHeap, 0, nBufferLen * sizeof(TCHAR));

	if (lpvParam == NULL)
	{
		data.m_pDlg->LogString(L"# ERROR - Pipe Server Failure:");
		data.m_pDlg->LogString(L"#    InstanceThread got an unexpected NULL value in lpvParam.");
		data.m_pDlg->LogString(L"#    InstanceThread exitting.");
		if (pBuffer != NULL) 
			HeapFree(hHeap, 0, pBuffer);
		return (DWORD)-1;
	}

	if (pBuffer == NULL)
	{
		data.m_pDlg->LogString(L"# ERROR - Pipe Server Failure:");
		data.m_pDlg->LogString(L"#    InstanceThread got an unexpected NULL heap allocation.");
		data.m_pDlg->LogString(L"#    InstanceThread exitting.");
		return (DWORD)-1;
	}

	data.m_pDlg->LogString(L"# InstanceThread created, receiving and processing messages.");

	BOOL bRead = FALSE;
	DWORD dwBytesRead = 0;
	while (true)
	{ 
		bRead = ReadFile(hPipe, pBuffer, nBufferLen, &dwBytesRead, NULL);         

		if (!bRead || dwBytesRead == 0)
		{   
			if (GetLastError() == ERROR_BROKEN_PIPE)
			{
				data.m_pDlg->LogString(L"# InstanceThread: client disconnected."); 
			}
			else
			{
				CString s;
				s.Format(L"# InstanceThread ReadFile failed, GLE=%d.", GetLastError());
				data.m_pDlg->LogString(s); 
			}
			break;
		}

		// Process the incoming message.
		CString sRequest;
		sRequest.Format(L"# Client request string: %s", pBuffer);
		data.m_pDlg->LogString(sRequest);
		
		CString sMessage;
		sMessage.Format(L"%s", pBuffer);
		int n = sMessage.Find(L"\r\n");
		if (n != -1)
			sMessage = sMessage.Left(n);
		
		ShellExecute(NULL, L"open", sMessage, L"", L"", SW_SHOWNORMAL);
	}

	FlushFileBuffers(hPipe); 
	DisconnectNamedPipe(hPipe); 
	CloseHandle(hPipe); 

	HeapFree(hHeap, 0, pBuffer);

	data.m_pDlg->LogString(L"# InstanceThread exitting.");
	return 1;
}



void CCrashReporterServerGuiDlg::PipeServerThread(IThread::TData dataPtr)
{
	const int nBufferLen = 512 * sizeof(TCHAR);
	ICancelable * cancelablePtr = dataPtr->GetCancelable();

	CString sPipename = L"\\\\.\\pipe\\mynamedpipe"; 
 
	while (true) 
	{ 
		if (cancelablePtr->IsCancel())
			break;

		LogString(L"# Pipe Server: Main thread awaiting client connection on " + sPipename);
		HANDLE hPipe = CreateNamedPipe(sPipename, PIPE_ACCESS_DUPLEX | FILE_FLAG_OVERLAPPED, PIPE_TYPE_MESSAGE | PIPE_READMODE_MESSAGE | PIPE_WAIT, PIPE_UNLIMITED_INSTANCES, 0, nBufferLen * sizeof(TCHAR), 0, NULL); 

		if (hPipe == INVALID_HANDLE_VALUE) 
		{
			CString s;
			s.Format(L"# CreateNamedPipe failed, GLE=%d.", GetLastError());
			LogString(s); 
			return;
		}
 
		HANDLE hEvent = CreateEvent(NULL, TRUE, TRUE, NULL);
		OVERLAPPED overlapped;
		overlapped.hEvent = hEvent;

		ConnectNamedPipe(hPipe, &overlapped);
		BOOL bCancelled = FALSE;
		while (true)
		{
			if (cancelablePtr->IsCancel())
			{
				bCancelled = TRUE;
				break;
			}
			DWORD dwRes = WaitForSingleObject(hEvent, 100);
			if (dwRes != WAIT_TIMEOUT)
				break;
		}

		if (bCancelled)
			break;

		CloseHandle(hEvent);

		DWORD dwBytes = 0;
		BOOL bConnected = GetOverlappedResult(hPipe, &overlapped, &dwBytes, FALSE) ? TRUE : (GetLastError() == ERROR_PIPE_CONNECTED);

		if (bConnected) 
		{ 
			LogString(L"# Client connected, creating a processing thread."); 
      
			CInstanceThreadData data;
			data.hPipe = hPipe;
			data.m_pDlg = this;

			DWORD dwThreadId = 0;
			HANDLE hThread = CreateThread(NULL, 0, InstanceThread, &data, 0, &dwThreadId);

			if (hThread == NULL) 
			{
				CString s;
				s.Format(L"# CreateThread failed, GLE=%d.", GetLastError());
				LogString(s); 
				return;
			}
			else 
			{
				CloseHandle(hThread);
			}
		} 
		else
		{
			CloseHandle(hPipe);
		}
	} 
}

void CCrashReporterServerGuiDlg::OnPipeServerThreadStarted()
{
	LogString(L"> Started pipe server");
}

void CCrashReporterServerGuiDlg::OnPipeServerThreadStopped()
{
	m_actionButton.EnableWindow();
	m_actionButton.SetWindowText(L"Start");
	LogString(L"> Stopped pipe server");
	GetThread(0)->Refresh();

	if (m_bReadyToExit)
	{
		Sleep(500);
		OnOK();
	}
}

// CCrashReporterServerGuiDlg dialog



CCrashReporterServerGuiDlg::CCrashReporterServerGuiDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CCrashReporterServerGuiDlg::IDD, pParent), m_bReadyToExit(FALSE)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CCrashReporterServerGuiDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT1, m_logEdit);
	DDX_Control(pDX, IDC_BUTTON1, m_actionButton);
}

BEGIN_MESSAGE_MAP(CCrashReporterServerGuiDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON2, &CCrashReporterServerGuiDlg::OnClickedButtonClose)
	ON_BN_CLICKED(IDC_BUTTON1, &CCrashReporterServerGuiDlg::OnClickedButtonAction)
	ON_MESSAGE(WM_USER + 123, &CCrashReporterServerGuiDlg::OnThreadSentData)
END_MESSAGE_MAP()


// CCrashReporterServerGuiDlg message handlers
LRESULT CCrashReporterServerGuiDlg::OnThreadSentData(WPARAM wParam, LPARAM lParam)
{
	return CThreadContainer::OnThreadSentData(wParam, lParam);
}

BOOL CCrashReporterServerGuiDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	SetIcon(m_hIcon, TRUE);
	SetIcon(m_hIcon, FALSE);

	AddThread((new CWinApiThread(GetSafeHwnd(), WM_USER + 123, delegates::make(this, &CCrashReporterServerGuiDlg::PipeServerThread)))
		->SetOnStarted(delegates::make(this, &CCrashReporterServerGuiDlg::OnPipeServerThreadStarted))
		->SetOnStopped(delegates::make(this, &CCrashReporterServerGuiDlg::OnPipeServerThreadStopped))
	);

	return TRUE;  // return TRUE  unless you set the focus to a control
}


void CCrashReporterServerGuiDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}


HCURSOR CCrashReporterServerGuiDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CCrashReporterServerGuiDlg::OnClickedButtonClose()
{
	if (GetThread(0)->GetState() != IThread::UndefinedState)
	{
		m_bReadyToExit = TRUE;
		GetThread(0)->Stop();
	}
	else
	{
		OnOK();
	}
}


void CCrashReporterServerGuiDlg::OnClickedButtonAction()
{
	if (GetThread(0)->GetState() == IThread::RunningState)
	{
		GetThread(0)->StopAsync();
		m_actionButton.EnableWindow(FALSE);
		LogString(L"> Stopping pipe server");
	}
	else
	{
		GetThread(0)->Run();
		m_actionButton.SetWindowText(L"Stop");
	}
}


void CCrashReporterServerGuiDlg::LogString(const CString & data)
{
	m_logEdit.SetSel(m_logEdit.GetWindowTextLength(), m_logEdit.GetWindowTextLength());
	m_logEdit.ReplaceSel(data + L"\r\n");
}
